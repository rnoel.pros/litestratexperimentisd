# LiteStrat Experiment
## For 29th International Conference on Information Systems Development

Experimental msterials and collected data for the initial validation of the LiteStrat modelling method.

Contents:

  * **materials**: Experimental materials (training slides, problems, survey results and model quality evaluation sheet) [in spanish]
  * **data**: SPSS data ().sav) and results report (.spv)
